package com.java;

public class Array {
	public static void main(String[] args) {

		   int[] numbers = {2, -9, 0, 5, 12, -25, 22, 9, 8, 12};
		   int sum = 0;
		   
		   // add each element in sum
		   for (int number: numbers) {
		     sum += number;
		   }
		  
		   int arrayLength = numbers.length;
		   System.out.println(arrayLength);
		   // calculate the average
		   // convert the average from int to double
		   double average;
		   average =  ((double) sum /(double) arrayLength);
		   System.out.println("Sum = " + sum);
		   System.out.println("Average = " + average);
		 }
}
