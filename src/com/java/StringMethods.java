package com.java;

public class StringMethods {

	public static void main(String[] args) {

		String string1 = "Hi this is Reshma";
		String string2 = new String("I love India");
		System.out.println(string1);
		System.out.println(string2);
		System.out.println(System.identityHashCode(string1));
		System.out.println(System.identityHashCode(string2));
		System.out.println(string1.length());
		boolean b = string1.endsWith(" Reshma");
		System.out.println(b);
		boolean c = string2.contains("this");
		System.out.println(c);
		String replace = string1.replace("Reshma", "Arshath");
		System.out.println(replace);
		
		boolean d = string1.contains("abi");
		System.out.println(d);
		String[] split = string1.split(" ");
		for (String string : split) {
			System.out.println(string);

		}
		String substring = string2.substring(3);
		System.out.println(substring);
		int indexOf = string1.indexOf("s");
		System.out.println(indexOf);
		int lastIndexOf = string1.lastIndexOf("d");
		System.out.println(lastIndexOf);
		
		String upperCase = string2.toUpperCase();
		System.out.println(upperCase);

		String lowerCase = string1.toLowerCase();
		System.out.println(lowerCase);

		


	}
	

}
