package com.java;

import java.util.ArrayList;
import java.util.List;

public class AssignmentArrayList {
	public static void main(String[] args) {

		List<Integer> li = new ArrayList<Integer>();
		List<Integer> li1 = new ArrayList<Integer>();

		li.add(1);
		li.add(2);
		li.add(3);
		li.add(3);
		li.add(5);
		li.add(6);
		li.add(7);
		li.add(8);
		li.add(9);
		li.add(9);
		li.add(8);
		li.add(9);
		li.add(1);
		li.add(4);
		int size = li.size();
		System.out.println(size);

		for (int i = 0; i < li.size(); i++) {
			System.out.println(li.get(i));
		}
		
		int x = li.get(3);
		System.out.println(x);
		li.add(8, 100);
		System.out.println(li);

		li.set(3, 10);
		System.out.println(li);
		li.remove(3);
		System.out.println(li);

		li.add(2, 20);
		System.out.println(li);

		li.set(5, 30);
		System.out.println(li);


		int inti = li.indexOf(7);
		System.out.println(inti);

		int index = li.lastIndexOf(20);
		System.out.println(index);

		li1.addAll(li);
		System.out.println(li);
		boolean b = li.contains(30);
		System.out.println(b);

		boolean y = li.contains(10);
		System.out.println(y);
		System.out.println(li1);
		
		li1.removeAll(li);
		System.out.println(li1);
		
		li1.retainAll(li);
		System.out.println(li1);
		
		li1.addAll(li);
		System.out.println(li);

	}


}
