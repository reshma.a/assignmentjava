package com.java;

import java.util.Collection;
import java.util.Set;
import java.util.TreeMap;

public class Assignmentmap {
	public static void main(String[] args) {
		TreeMap<Character, Integer> m = new TreeMap<>();
		m.put('R', 1);
		m.put('E', 2);
		m.put('S', 3);
		m.put('H', 4);
		m.put('M', 5);
		m.put('A', 6);
		m.put('S', 3);
		m.put('S', 7);
		System.out.println(m);
		
		Collection<Integer> values = m.values();
		for (Integer integer : values) {
			System.out.println(integer);
		}
		
		Set<Character> keySet = m.keySet();
		System.out.println(keySet);
		for (Character character : keySet) {
			System.out.println(character);

		}
		m.put('6', 20);
		System.out.println(m);
		
		Set<java.util.Map.Entry<Character, Integer>> keySet2 = m.entrySet();
		for (java.util.Map.Entry<Character, Integer> character1 : keySet2) {
			System.out.println(character1);

		}
		
	}

}
